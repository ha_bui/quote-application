package com.project.quoteclient.controller;

import com.google.gson.Gson;
import com.nitorcreations.junit.runners.NestedRunner;
import com.project.quoteclient.QuoteclientApplication;
import com.project.quoteclient.model.Quote;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = QuoteclientApplication.class)
public class UserControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    private String URI_REGISTER = "/customers/register";
    private String URI_ADD_QUOTE = "/customers/addQuote";
    private String URI_GET_ALL_QUOTES = "/customers/quotes";
    private String URI_GET_RANDOM = "/customers/random";
    private String URI_UPDATE_QUOTE = "/customers/update";

    private String JSON_USER_CONTENT = "{\"userName\": \"blngocha123\",\"email\": \"blngocha123@gmail.com\",\"mobile\": \"0916902063\",\"address\": \"VN\",\"password\": \"ha123\"}";

    private String NEW_QUOTE = "{\"id\": 7,\"author\": \"Mark Twain_123\",\"content\": \"The secret of getting ahead is getting started.\"}";

    private String HEADER_TYPE = "Authorization";
    private String HEADER_VALUE = "Basic YWRtaW46YWRtaW4=";
    private String TOKEN = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJlMWY3M2MxYy1hZTQ0LTQ1MzYtYWU3MC01ZDIwZmU5MGZmOWQiLCJpYXQiOjE2MTQwMTc4NjEsImV4cCI6MTYxNDAxODE2MSwidXNlck5hbWUiOiJuZ29jaGEiLCJyb2xlIjoiY3VzdG9tZXIifQ.vcu3HkS9b42DEVNlF4V95cXDcaCJ1Ck5ZEmcYhRgZIDQB8hZeKneSLHhR8mU6ULKM5Pgh7uj5zBCmmnaNk0waA";

    @Mock
    Quote quote;

    @Before
    public void setUp(){
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    public void testRegister() throws Exception{
        MvcResult result = this.mockMvc.perform(MockMvcRequestBuilders.post(URI_REGISTER)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSON_USER_CONTENT)
                .header(HEADER_TYPE,HEADER_VALUE)
                .accept(MediaType.APPLICATION_JSON))
                .andReturn();
        int status = result.getResponse().getStatus();
        assertEquals(201, status);
    }

    @Test
    public void testAddQuote() throws Exception{
        MvcResult result = this.mockMvc.perform(MockMvcRequestBuilders.post(URI_ADD_QUOTE)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(NEW_QUOTE)
                        .header(HEADER_TYPE,TOKEN)
                        .accept(MediaType.APPLICATION_JSON))
                        .andReturn();
        int status = result.getResponse().getStatus();
        assertEquals(201, status);
    }

    @Test
    public void testGetAllQuotes() throws Exception{
        MvcResult result = this.mockMvc.perform(MockMvcRequestBuilders.get(URI_GET_ALL_QUOTES)
                        .header(HEADER_TYPE,TOKEN)
                        .accept(MediaType.APPLICATION_JSON)).andReturn();
        assertEquals(200, result.getResponse().getStatus());
    }

    @Test
    public void testGetRandom() throws Exception{
        MvcResult result = this.mockMvc.perform(MockMvcRequestBuilders.get(URI_GET_RANDOM)
                        .header(HEADER_TYPE, TOKEN)
                        .accept(MediaType.APPLICATION_JSON)).andReturn();
        assertEquals(200, result.getResponse().getStatus());
    }

    /*@Test
    public void testUpdateQuote() throws Exception{
        Gson gson = new Gson();
        String jsonQuote = gson.toJson(quote, Quote.class);
        MvcResult result = this.mockMvc.perform((MockMvcRequestBuilders.put(URI_UPDATE_QUOTE))
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(jsonQuote)
                            .header(HEADER_TYPE,TOKEN)
                            .accept(MediaType.APPLICATION_JSON)).andReturn();
        int status = result.getResponse().getStatus();
        assertEquals(200, status);
    }*/
}
