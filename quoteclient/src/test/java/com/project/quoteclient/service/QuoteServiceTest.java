package com.project.quoteclient.service;

import com.project.quoteclient.QuoteclientApplication;
import com.project.quoteclient.model.Quote;
import com.project.quoteclient.repository.QuoteRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {QuoteclientApplication.class})
public class QuoteServiceTest {

    @InjectMocks
    QuoteService quoteServiceMock;

    @Mock
    Quote quote;

    @Mock
    QuoteRepository quoteRepositoryMock;

    @Before
    public void setUp(){
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetAllQuotes(){
        when(quoteRepositoryMock.findAll()).thenReturn(List.of(quote));
    }

    @Test
    public void testAddQuote(){
        when(quoteRepositoryMock.save(quote)).thenReturn(quote);
    }

    @Test
    public void testAddListQuote(){
        when(quoteRepositoryMock.saveAll(List.of(quote))).thenReturn(List.of(quote));
    }

}
