package com.project.quoteclient.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValidateResponse {

    private String userId;
    private String userName;
    private String roles;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getRoles() {
		return roles;
	}
	public void setRoles(String roles) {
		this.roles = roles;
	}
    
    
}
