package com.project.quoteclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuoteclientApplication {

    public static void main(String[] args) {
        SpringApplication.run(QuoteclientApplication.class, args);
    }

}
