package com.project.quoteclient.repository;

import com.project.quoteclient.model.Quote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface QuoteRepository extends JpaRepository<Quote, Long> {

    List<Quote> findByAuthor(String name);

    @Modifying
    @Query(value="DELETE FROM Quote q where q.author = :name", nativeQuery = true)
    void deleteByAuthor(@Param("name") String name);

    @Query(value="SELECT * FROM quote ORDER BY RAND() LIMIT 1", nativeQuery = true)
    Quote findRandom();
}
