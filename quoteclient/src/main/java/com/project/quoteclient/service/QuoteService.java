package com.project.quoteclient.service;

import com.project.quoteclient.model.Quote;
import com.project.quoteclient.repository.QuoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class QuoteService {

    @Autowired
    private QuoteRepository repository;

    public Quote saveQuote(Quote q){
        return repository.save(q);
    }
    public List<Quote> saveQuotes(List<Quote> lstQuote) {
        return repository.saveAll(lstQuote);
    }

    public List<Quote> getQuotes(){
        return repository.findAll();
    }

    public List<Quote> getQuoteByAuthor(String name){
        return repository.findByAuthor(name);
    }

    public String deleteQuote(String name) {
        repository.deleteByAuthor(name);
        return "Quote removed !";
    }

    public Quote findRandom(){
        return repository.findRandom();
    }

    public Quote updateContentOfQuote(Quote quote){
        Optional<Quote> existingQuote = repository.findById(quote.getId());
        if(Objects.nonNull(existingQuote)){
            Quote q = existingQuote.get();
            q.setAuthor(quote.getAuthor());
            q.setContent(quote.getContent());
            return repository.save(q);
        } else {
            return null;
        }

    }
}
