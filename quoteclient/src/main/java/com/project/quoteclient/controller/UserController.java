package com.project.quoteclient.controller;
import com.project.quoteclient.config.ConfigUtils;
import com.project.quoteclient.model.AuthToken;
import com.project.quoteclient.model.CustomerRegistrationRequest;
import com.project.quoteclient.model.LoginRequest;
import com.project.quoteclient.model.Quote;
import com.project.quoteclient.service.QuoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping("/customers")
public class UserController {

    @Autowired
    private ConfigUtils configUtils;

    @Autowired
    private QuoteService service;

    private RestTemplate restTemplate;

    @Autowired
    public UserController(RestTemplateBuilder builder) {
        this.restTemplate = builder.build();
    }

    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    public AuthToken register(@RequestBody CustomerRegistrationRequest user) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, configUtils.getBasicAuth());

        HttpEntity<CustomerRegistrationRequest> entity = new HttpEntity<>(user, headers);

        return restTemplate.exchange(configUtils.getRegistrationUrl(), HttpMethod.POST, entity, AuthToken.class).getBody();
    }

    @PostMapping("/login")
    @ResponseStatus(HttpStatus.OK)
    public AuthToken login(@RequestBody LoginRequest loginDto) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, configUtils.getBasicAuth());
        HttpEntity<LoginRequest> entity = new HttpEntity<>(loginDto, headers);
        return restTemplate.exchange(configUtils.getLoginUrl(), HttpMethod.POST, entity, AuthToken.class).getBody();
    }

    @PostMapping("/addQuote")
    @ResponseStatus(HttpStatus.CREATED)
    public Quote addQuote(@RequestBody Quote quote){
        return service.saveQuote(quote);
    }

    @GetMapping("/quotes")
    @ResponseStatus(HttpStatus.OK)
    public List<Quote> findAllQuotes(){
        return service.getQuotes();
    }

    @GetMapping("/quote/{name}")
    @ResponseStatus(HttpStatus.OK)
    public List<Quote> findProductByAuthor(@PathVariable String name){
        if(name.contains("+")){
            name = name.replace("+", " ");
        } else if(name.contains("-")){
            name = name.replace("-"," ");
        }
        return service.getQuoteByAuthor(name);
    }

    @GetMapping("/random")
    @ResponseStatus(HttpStatus.OK)
    public Quote findRandom(){
        return service.findRandom();
    }

    @PutMapping("/update")
    @ResponseStatus(HttpStatus.OK)
    public Quote updateQuote(@RequestBody Quote quote){
        return service.updateContentOfQuote(quote);
    }

    @DeleteMapping("/delete")
    @ResponseStatus(HttpStatus.OK)
    public String deleteQuote(@RequestParam String author){
        return service.deleteQuote(author);
    }
}
