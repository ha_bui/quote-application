package com.project.quoteserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuoteserverApplication {

    public static void main(String[] args) {
        SpringApplication.run(QuoteserverApplication.class, args);
    }

}
