package com.project.quoteserver.controller;

import com.project.quoteserver.QuoteserverApplication;
import com.project.quoteserver.model.User;
import com.project.quoteserver.repository.UserRepo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.junit.Assert.assertEquals;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = {QuoteserverApplication.class})
public class UserControllerTest {

    private MockMvc mockMvc;

    @Mock
    private UserRepo userRepo;

    @Mock
    private User user;

    @Mock
    private User userExisting;

    @Autowired
    private WebApplicationContext wac;

    private String URI_REGISTER = "/auth/user/register";
    private String JSON_USER_CONTENT = "{\"userName\": \"blngocha123\",\"email\": \"blngocha123@gmail.com\",\"mobile\": \"0916902063\",\"address\": \"VN\",\"password\": \"ha123\"}";

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    public void testRegisterUser() throws Exception {
        MvcResult result = this.mockMvc.perform(MockMvcRequestBuilders.post(URI_REGISTER)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSON_USER_CONTENT)
                .accept(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8"))
                .andReturn();
        int status = result.getResponse().getStatus();
        assertEquals(201, status);
    }
}
